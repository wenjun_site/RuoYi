package com.ruoyi.web.controller.fund;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.fund.domain.Fund;
import com.ruoyi.fund.service.IFundService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 基金收益Controller
 * 
 * @author caowenjun
 * @date 2020-08-19
 */
@Controller
@RequestMapping("/fund/fund")
public class FundController extends BaseController
{
    private String prefix = "fund/fund";

    @Autowired
    private IFundService fundService;

    @RequiresPermissions("fund:fund:view")
    @GetMapping()
    public String fund()
    {
        return prefix + "/fund";
    }

    /**
     * 查询基金收益列表
     */
    @RequiresPermissions("fund:fund:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Fund fund)
    {
        startPage();
        List<Fund> list = fundService.selectFundList(fund);
        return getDataTable(list);
    }

    /**
     * 导出基金收益列表
     */
    @RequiresPermissions("fund:fund:export")
    @Log(title = "基金收益", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Fund fund)
    {
        List<Fund> list = fundService.selectFundList(fund);
        ExcelUtil<Fund> util = new ExcelUtil<Fund>(Fund.class);
        return util.exportExcel(list, "fund");
    }

    /**
     * 新增基金收益
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存基金收益
     */
    @RequiresPermissions("fund:fund:add")
    @Log(title = "基金收益", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Fund fund)
    {
        return toAjax(fundService.insertFund(fund));
    }

    /**
     * 修改基金收益
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        Fund fund = fundService.selectFundById(id);
        mmap.put("fund", fund);
        return prefix + "/edit";
    }

    /**
     * 修改保存基金收益
     */
    @RequiresPermissions("fund:fund:edit")
    @Log(title = "基金收益", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Fund fund)
    {
        return toAjax(fundService.updateFund(fund));
    }

    /**
     * 删除基金收益
     */
    @RequiresPermissions("fund:fund:remove")
    @Log(title = "基金收益", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(fundService.deleteFundByIds(ids));
    }
}
