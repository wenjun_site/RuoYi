package com.ruoyi.fund.service;

import java.util.List;
import com.ruoyi.fund.domain.Fund;

/**
 * 基金收益Service接口
 * 
 * @author caowenjun
 * @date 2020-08-19
 */
public interface IFundService 
{
    /**
     * 查询基金收益
     * 
     * @param id 基金收益ID
     * @return 基金收益
     */
    public Fund selectFundById(Long id);

    /**
     * 查询基金收益列表
     * 
     * @param fund 基金收益
     * @return 基金收益集合
     */
    public List<Fund> selectFundList(Fund fund);

    /**
     * 新增基金收益
     * 
     * @param fund 基金收益
     * @return 结果
     */
    public int insertFund(Fund fund);

    /**
     * 修改基金收益
     * 
     * @param fund 基金收益
     * @return 结果
     */
    public int updateFund(Fund fund);

    /**
     * 批量删除基金收益
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFundByIds(String ids);

    /**
     * 删除基金收益信息
     * 
     * @param id 基金收益ID
     * @return 结果
     */
    public int deleteFundById(Long id);
}
