package com.ruoyi.fund.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fund.mapper.FundMapper;
import com.ruoyi.fund.domain.Fund;
import com.ruoyi.fund.service.IFundService;
import com.ruoyi.common.core.text.Convert;

/**
 * 基金收益Service业务层处理
 * 
 * @author caowenjun
 * @date 2020-08-19
 */
@Service
public class FundServiceImpl implements IFundService 
{
    @Autowired
    private FundMapper fundMapper;

    /**
     * 查询基金收益
     * 
     * @param id 基金收益ID
     * @return 基金收益
     */
    @Override
    public Fund selectFundById(Long id)
    {
        return fundMapper.selectFundById(id);
    }

    /**
     * 查询基金收益列表
     * 
     * @param fund 基金收益
     * @return 基金收益
     */
    @Override
    public List<Fund> selectFundList(Fund fund)
    {
        return fundMapper.selectFundList(fund);
    }

    /**
     * 新增基金收益
     * 
     * @param fund 基金收益
     * @return 结果
     */
    @Override
    public int insertFund(Fund fund)
    {
        return fundMapper.insertFund(fund);
    }

    /**
     * 修改基金收益
     * 
     * @param fund 基金收益
     * @return 结果
     */
    @Override
    public int updateFund(Fund fund)
    {
        return fundMapper.updateFund(fund);
    }

    /**
     * 删除基金收益对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteFundByIds(String ids)
    {
        return fundMapper.deleteFundByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除基金收益信息
     * 
     * @param id 基金收益ID
     * @return 结果
     */
    @Override
    public int deleteFundById(Long id)
    {
        return fundMapper.deleteFundById(id);
    }
}
