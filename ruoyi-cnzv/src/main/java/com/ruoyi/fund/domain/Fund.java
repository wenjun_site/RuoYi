package com.ruoyi.fund.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 基金收益对象 cvb_fund
 * 
 * @author caowenjun
 * @date 2020-08-19
 */
public class Fund extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 流水号 */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private String userCode;

    /** 基金id */
    @Excel(name = "基金id")
    private String fundCode;

    /** 基金名称 */
    @Excel(name = "基金名称")
    private String fundName;

    /** 持仓 */
    @Excel(name = "持仓")
    private BigDecimal principal;

    /** 份额 */
    @Excel(name = "份额")
    private BigDecimal shares;

    /** 总额 */
    @Excel(name = "总额")
    private BigDecimal amount;

    /** 当天收益 */
    @Excel(name = "当天收益")
    private BigDecimal curIncome;

    /** 最新净值 */
    @Excel(name = "最新净值")
    private String nextworth;

    /** 当前估值 */
    @Excel(name = "当前估值")
    private String expectworth;

    /** 增长率 */
    @Excel(name = "增长率")
    private String expectgrowth;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserCode(String userCode) 
    {
        this.userCode = userCode;
    }

    public String getUserCode() 
    {
        return userCode;
    }
    public void setFundCode(String fundCode) 
    {
        this.fundCode = fundCode;
    }

    public String getFundCode() 
    {
        return fundCode;
    }
    public void setFundName(String fundName) 
    {
        this.fundName = fundName;
    }

    public String getFundName() 
    {
        return fundName;
    }
    public void setPrincipal(BigDecimal principal) 
    {
        this.principal = principal;
    }

    public BigDecimal getPrincipal() 
    {
        return principal;
    }
    public void setShares(BigDecimal shares) 
    {
        this.shares = shares;
    }

    public BigDecimal getShares() 
    {
        return shares;
    }
    public void setAmount(BigDecimal amount) 
    {
        this.amount = amount;
    }

    public BigDecimal getAmount() 
    {
        return amount;
    }
    public void setCurIncome(BigDecimal curIncome) 
    {
        this.curIncome = curIncome;
    }

    public BigDecimal getCurIncome() 
    {
        return curIncome;
    }
    public void setNextworth(String nextworth) 
    {
        this.nextworth = nextworth;
    }

    public String getNextworth() 
    {
        return nextworth;
    }
    public void setExpectworth(String expectworth) 
    {
        this.expectworth = expectworth;
    }

    public String getExpectworth() 
    {
        return expectworth;
    }
    public void setExpectgrowth(String expectgrowth) 
    {
        this.expectgrowth = expectgrowth;
    }

    public String getExpectgrowth() 
    {
        return expectgrowth;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userCode", getUserCode())
            .append("fundCode", getFundCode())
            .append("fundName", getFundName())
            .append("principal", getPrincipal())
            .append("shares", getShares())
            .append("amount", getAmount())
            .append("curIncome", getCurIncome())
            .append("nextworth", getNextworth())
            .append("expectworth", getExpectworth())
            .append("expectgrowth", getExpectgrowth())
            .toString();
    }
}
